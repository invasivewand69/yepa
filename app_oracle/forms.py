from django.forms import fields
from .models import Producto
from django import forms

class ProductoForm(forms.ModelForm):
    
    nombre_prod = forms.CharField(label='Titulo del producto:')
    imagen_prod = forms.ImageField()
    
    class Meta:
        model= Producto
        fields = ["id_prod", "nombre_prod", "descripcion_prod", "precio_prod", "imagen_prod", "metpago_prod", "categoria_prod"]