from app_oracle.models import Producto
from django.contrib import admin

# Register your models here.
class admProducto(admin.ModelAdmin):
    list_display=["id_prod", "nombre_prod", "descripcion_prod", "precio_prod", "imagen_prod", "metpago_prod", "categoria_prod"]
    
    class Meta:
        model= Producto
        
admin.site.register(Producto, admProducto)