from django.urls import path
from .views import ayuda, baseSeccionPerfil, carrito, categorias, catvideojuegos, deseos, hollow, index, joycon, mando, pedidos, producto, vender, vender_producto

urlpatterns = [
    path('', index, name='index'),
    path('categorias/', categorias, name='categorias'),
    path('pedidos/', pedidos, name='pedidos'),
    path('vender/', vender, name='vender'),
    path('ayuda/', ayuda, name='ayuda'),
    path('carrito/', carrito, name='carrito'),
    path('deseos/', deseos, name='deseos'),
    path('videojuegos/', catvideojuegos, name='catvideojuegos'),
    path('mando/', mando, name='mando'),
    path('hollow/', hollow, name='hollow'),
    path('joycon/', joycon, name='joycon'),
    path('producto/', producto, name='producto'),
    path('baseSeccionPerfil/', baseSeccionPerfil, name='baseSeccionPerfil'),
    path('venderProducto/', vender_producto, name='venderProducto')
]
