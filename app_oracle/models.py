from django.db import models

# Create your models here.
Metodo_pago = [
    ('1','Visa'),
    ('2', 'Paypal'),
    ('3', 'Efectivo'),
    ('4', 'Transferencia'),
]

Categorias = [
    ('1', 'Deporte'),
    ('2', 'Tecnología'),
    ('3', 'Vehiculos'),
    ('4', 'Moda hombre'),
    ('5', 'Moda mujer'),
    ('6', 'Videojuegos'),
    ('7', 'Instrumentos musicales'),
    ('8', 'Electrodomesticos'),
    ('9', 'Cocina'),
    ('10', 'Baño'),
    ('11', 'Hogar y muebles'),
]
    
class Producto(models.Model):
    id_prod = models.IntegerField(primary_key=True)
    nombre_prod = models.CharField(max_length=60, null=False)
    descripcion_prod = models.CharField(max_length=600, null=False)
    precio_prod = models.IntegerField(null=False)
    imagen_prod = models.ImageField(upload_to='Productos', null=False)
    metpago_prod = models.CharField(max_length=30, choices=Metodo_pago, default='1')
    categoria_prod = models.CharField(max_length=60, choices=Categorias, default='1') 
    
    def __str__(self):
        return f"{self.id_prod} - {self.nombre_prod} - {self.precio_prod} - {self.categoria_prod}"