function sololetras(e){

    key=e.keycode || e.which;

    teclado= String.fromCharCode(key).toLowerCase();

    letras=" abcdefghijkmnñopqrstuvxyzáéíóúüö";



    if(letras.indexOf(teclado)==-1)

    {

        return false;

    }

}
/*Seccion dedicada al tema de los contadores de productos en el carrito de compra*/ 
/*Obtenemos acceso a las etiquetas HTML y las guardamos en variables*/
const h4contador1 = document.getElementsByClassName("contadorProductos")[0];
const h4contador2 = document.getElementsByClassName("contadorProductos")[1];
const h4contador3 = document.getElementsByClassName("contadorProductos")[2];
const botonAumentar1 = document.getElementsByClassName("increaseButton")[0];
const botonAumentar2 = document.getElementsByClassName("increaseButton")[1];
const botonAumentar3 = document.getElementsByClassName("increaseButton")[2];
const botonDisminuir1 = document.getElementsByClassName("decreaseButton")[0];
const botonDisminuir2 = document.getElementsByClassName("decreaseButton")[1];
const botonDisminuir3 = document.getElementsByClassName("decreaseButton")[2];
/*Declaramos la variable cantidad producato para uasrlo en el H4 e ir aumentando y disminuyendo*/ 
var cantidadProductoA = 1;
var cantidadProductoB = 1;
var cantidadProductoC = 1;
/*Agregamos los listeners y funciones para aumentar y disminuir la cantidad de productos*/ 
botonAumentar1.addEventListener("click", () => {
    cantidadProductoA += 1;
    h4contador1.innerHTML = cantidadProductoA;
    
});
botonAumentar2.addEventListener("click", () => {
    cantidadProductoB += 1;
    h4contador2.innerHTML = cantidadProductoB;
});
botonAumentar3.addEventListener("click", () => {
    cantidadProductoC += 1;
    h4contador3.innerHTML = cantidadProductoC;
    
});

botonDisminuir1.addEventListener("click", () => {
    if(cantidadProductoA>1){
        cantidadProductoA -= 1;
        h4contador1.innerHTML = cantidadProductoA;
    }
});
botonDisminuir2.addEventListener("click", () => {
    if(cantidadProductoB>1){
        cantidadProductoB -= 1;
        h4contador2.innerHTML = cantidadProductoB;
    }
});
botonDisminuir3.addEventListener("click", () => {
    if(cantidadProductoC>1){
        cantidadProductoC -= 1;
        h4contador3.innerHTML = cantidadProductoC;
    }
});

/*Marcar una direccion con la API de Google maps*/ 
function iniciarMap(){
    var coord = {lat:-33.4488897 ,lng: -70.6692655};
    var map = new google.maps.Map(document.getElementById('map'),{
      zoom: 10,
      center: coord
    });
    var marker = new google.maps.Marker({
      position: coord,
      map: map
    });
}

function iniciarMapB(){
    var coord = {lat:-23.6566311 ,lng: 70.3999336};
    var map = new google.maps.Map(document.getElementById('map2'),{
      zoom: 10,
      center: coord
    });
    var marker = new google.maps.Marker({
      position: coord,
      map: map
    });
}


/*Marcar una direccion con la API de Google maps*/ 
function iniciarMapA(){
    var coord = {lat:-36.835921 ,lng:  -73.144785};
    var map1 = new google.maps.Map(document.getElementById('map1'),{
      zoom: 10,
      center: coord
    });
    var marker = new google.maps.Marker({
      position: coord,
      map1: map1
    });
}
