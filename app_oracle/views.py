from app_oracle.forms import ProductoForm
from django.shortcuts import redirect, render

# Create your views here.
def index(request):
    return render(request, "app_oracle/index.html")

def categorias(request):
    return render(request, "app_oracle/categorias.html")

def pedidos(request):
    return render(request, "app_oracle/mispedidos.html")

def vender(request):
    return render(request, "app_oracle/vender_producto.html")

def ayuda(request):
    return render(request, "app_oracle/ayudausuario.html")

def carrito(request):
    return render(request, "app_oracle/carrito de compras.html")

def deseos(request):
    return render(request, "app_oracle/lista de deseos.html")

def catvideojuegos(request):
    return render(request, "app_oracle/categoriavideojuegos.html")

def mando(request):
    return render(request, "app_oracle/producto mando.html")

def hollow(request):
    return render(request, "app_oracle/producto hollow.html")

def joycon(request):
    return render(request, "app_oracle/producto joycon.html")

def producto(request):
    return render(request, "app_oracle/click_producto.html")

def baseSeccionPerfil(request):
    return render(request, "app_oracle/BaseSeccionPerfil.html")

def vender_producto(request):
    formulario = ProductoForm(request.POST or None)
    
    contexto= {
        'form': formulario 
    }
    
    if request.method == "POST":
        form = ProductoForm(data=request.POST, files=request.FILES)
        
        if form.is_valid():
            form.save()
            return redirect(to="venderProducto")
    return render(request, 'app_oracle/vender_producto.html', contexto)